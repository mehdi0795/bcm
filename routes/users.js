const express = require('express');
const router = express.Router();
const {processError} = require('../utils/utils');
const registerHandler = require('../api/users/register');
const authenticateHandler = require('../api/users/authenticate');

/**
 * POST: Create a new User
 */
router.post('/register', async (req, res, next) => {
    try {
        console.log('here zab', req.body);
        res.status(200).json(await registerHandler(req.body));
    } catch (err) {
        processError(err, next);
    }
});

/**
 * POST: Authentication route
 */
router.post('/authenticate', async (req, res, next) => {
    try {
        res.status(200).json(await authenticateHandler(req.body));
    } catch (err) {
        processError(err, next);
    }
});

module.exports = router;
