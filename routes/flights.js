const express = require('express');
const router = express.Router();
const {processError} = require('../utils/utils');
const getBestFlights = require('../api/flights/get');
const authMiddleware = require('../utils/middleware');

router.get('/', authMiddleware, async (req, res, next) => {
    try {
        res.status(200).json(await getBestFlights(res));
    } catch (err) {
        processError(err, next);
    }
});

module.exports = router;
