# Test-technique-BCM

Ce projet est réalisé en tant que test technique pour BCM.  

Ce projet a été géneré avec [Express.js Generator](https://github.com/expressjs/generator) (BACKEND).

## Installation et lancement du serveur de la base de données MongoDB

Le serveur (projet backend) communique avec une base de données MongoDB.

## Guide d'installation/exécution

1- Ouvrir le dossier TestBCM. 
2- Installer les dépendances en exécutant la commande `npm install`.  
3- Executer la commande `npm start` ou `nodemon` pour lancer le serveur.  
4- Ouvrir Postman ou autre pour tester les requêtes.  
5- Avant d'envoyer  GET `/api/flights` sur l'API vous devriez créer un utilisateur en envoyant POST `/api/users/register`, le modèle utilisateur est composé d'un `email` et d'un `password` que vous devez mettre dans le body.  
6- Après la création d'un compte vous devriez vous authentifier en envoyant POST `/api/users/authenticate` et récupérer le Token.   
7- Pour tester la requête GET `/api/flights` vous devriez choisir le type Bearer Token et insérer le Token récupéré.  

