const createError = require('http-errors');
var bcrypt = require('bcryptjs');
const User = require('../../models/User');
const _ = require('lodash');

const registerHandler = async userData => {
    if (_.isEmpty(userData)) {
        throw createError(400, 'No data posted. Please fill some data !');
    }
    
    const {email} = (userData || {});
    
    const existingUser = await User.findOne({email});
    if (existingUser) {
        throw createError(409, 'This email already exists for another User');
    }
    const newUser = new User({
        ...userData,
        password: bcrypt.hashSync(userData.password
            , bcrypt.genSaltSync())
    });
    
    const savedUser = await newUser.save();
    return savedUser;
};

module.exports = registerHandler;
