const createError = require('http-errors');
var bcrypt = require('bcryptjs');
const User = require('../../models/User');
const jwt = require('jsonwebtoken');

const authenticateHandler = async userData => {

    const foundUser = await User.findOne({ email: userData.email });
    if(!foundUser) {
        throw createError(400, 'Email not found');
    }

    if (bcrypt.compareSync(userData.password, foundUser.password)) 
    {
        const token = jwt.sign({ email: userData.email }
            , 'secret'
            , { expiresIn: '24h' });
        const creditentials = {
            foundUser,
            
            token
        };
        return creditentials;
    }
    else {
        throw createError(400,'Wrong password');
    }
};

module.exports = authenticateHandler;
