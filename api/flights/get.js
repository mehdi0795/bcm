const Flight = require('../../models/Flight');
var request = require('request-promise');
const csv2json = require('csvjson-csv2json');

const deleteAllPreviousFlights = async () => {
    await Flight.collection.drop();
};

const getFlightsFromApi = async (res, companiesInfo) => {
    try {
        let flightsArray = companiesInfo.map(async company => {
            return new Promise(function (resolve, reject) {
                const rp = request(company.url).then(function (body) {
                    let allFlights;
                    try {
                        allFlights = JSON.parse(body);
                    } catch (error) {
                        allFlights = csv2json(body, { parseNumbers: true });
                    }
                    const promises = (allFlights || []).map(
                        flight =>
                            // eslint-disable-next-line no-async-promise-executor
                            new Promise(async resolve => {
                                const newFlight = new Flight({
                                    provider: company.provider,
                                    price: flight[Object.keys(flight)[1]],
                                    departure_time: flight[Object.keys(flight)[2]],
                                    arrival_time: flight[Object.keys(flight)[3]]
                                });
                                const createdFlight = await newFlight.save();
                                resolve(createdFlight);
                            })
                    );
                    Promise.all(promises);
                });
                resolve(rp);
            });
        });
        const resolvedFlights = await Promise.all(flightsArray);
        return resolvedFlights;
    } catch (err) {
        res.statusCode = 500;
        res.end();
    }
};

const getAllflights = async res => {
    const foundFlights = await Flight.find();
    if (foundFlights.length != 0) {
        await deleteAllPreviousFlights();
    }

    const companiesInfo = [
        {
            url: 'https://my.api.mockaroo.com/air-jazz/flights?key=dd764f40',
            provider: 'Air Jazz'
        },
        {
            url: 'https://my.api.mockaroo.com/air-moon/flights?key=dd764f40',
            provider: 'Air Moon'
        },
        {
            url: 'https://my.api.mockaroo.com/air-beam/flights?key=dd764f40',
            provider: 'Air Beam'
        }
    ];
    await getFlightsFromApi(res, companiesInfo);
};

const getBestFlights = async res => {
    await getAllflights(res);
    const flights = await Flight.aggregate([
        { $sort: { price: 1 } },
        { $limit: 50 }
    ]).exec();
    return flights;
};

module.exports = getBestFlights;
