const mongoose = require('mongoose');
var valid = require('validator');

const userSchema = new mongoose.Schema({
    email : {
        type : String,
        required : true,
        minlength: 1,
        trim : true,
        validate:{
            validator:valid.isEmail,
            message:'{VALUE} Email is not valid'}
    },   
    password: String
}, {collection : 'users'});

module.exports = mongoose.model('User', userSchema);
