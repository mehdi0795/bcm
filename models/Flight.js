const mongoose = require('mongoose');

const flightSchema = new mongoose.Schema({
    provider: String,
    price: Number,
    departure_time: String,
    arrival_time: String
}, {collection : 'flights'});

module.exports = mongoose.model('Flight', flightSchema);
