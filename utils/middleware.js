const jwt = require('jsonwebtoken');
const { processError } = require('../utils/utils');


module.exports = (req, res, next) => {
    const header = req.headers.authorization;
    try {
        
        if (header === undefined) return res.status(401).send('Must be connected');

        const token = header.split(' ')[1];
        const decodedToken = jwt.verify(token, 'secret');
        if (decodedToken) {
            next();
        }
    } catch (err) {
        processError(err, next);
    }
};
